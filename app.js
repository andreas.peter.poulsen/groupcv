
var express = require('express');
var path = require('path');

var http = require('http');
var fs = require('fs'); //Read file?


var app = express();
app.listen(process.env.PORT || 8080);


(function init() {
    routes()
})();


function routes() {
    app.use(express.static(path.join(__dirname + "/public")));
    app.use(express.static(path.join(__dirname + "/routes")));

    app.get('/', (req,res ) => res.sendfile(path.join(__dirname + '/routes/indexPage/index.html')));
    app.get('/data', (req,res ) => res.sendfile(path.join(__dirname + '/public/json/data.json')));

}





